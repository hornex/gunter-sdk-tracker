var Gunter = /** @class */ (function () {
    function Gunter(apiUrl) {
        this.apiUrl = apiUrl;
    }
    Gunter.prototype.dispatch = function (event) {
        fetch(this.apiUrl, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(event),
        });
    };
    Gunter.prototype.init = function () {
        var _this = this;
        if (window === undefined) {
            console.error("Window is undefined");
        }
        document.addEventListener("keydown", function (event) {
            _this.dispatch({
                key: event.key,
                code: event.code,
                time: Date.now(),
            });
        });
        console.log("Gunter initialized");
    };
    return Gunter;
}());
export default Gunter;
//# sourceMappingURL=index.js.map